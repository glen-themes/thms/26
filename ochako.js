// reassign npf post types
document.addEventListener("DOMContentLoaded", () => {
    npfIcon("[data-npf*='audio']", "audio", "sf-sound-o");
    npfIcon("[data-npf*='poll']", "poll", "sf-graph-3-o");
    npfIcon(".photo-origin:not(:has([data-npf*='video']))", "photo", "sf-picture-o");
    npfIcon(".pp > .npf_inst:first-child", "photo", "sf-picture-o");
    npfIcon(".photo-origin:has([data-npf*='video'])", "video", "sf-video-o");
    npfIcon(".npf_inst .tmblr-full audio[controls]", "audio", "sf-sound-o")
})

function npfIcon(type, typeName, iconName){
    let p_p = document.querySelectorAll(".pp");
    p_p ? p_p.forEach(pp => {
        let p_aud = pp.querySelector(type);
        if(p_aud){
            let tp = pp.closest(".posts").querySelector(".citrus");
            if(tp){
                let at = tp.querySelector("a[href][title]");
                let at_txt = at.getAttribute("title");
                at.setAttribute("title",typeName + " " + at_txt.split(" ").slice(1).join(" "))
                
                tp.querySelector(".sf").classList.remove("sf-font")
                tp.querySelector(".sf").classList.add(iconName)
            }
        }
    }) : "";
}
