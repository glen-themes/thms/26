![Screenshot preview of the theme "Nebulae" by glenthemes](https://64.media.tumblr.com/9826db02c62f0fdb13d09f7949c59cf9/tumblr_p3crniLU6J1ubolzro2_540.gif)

**Theme no.:** 26  
**Theme name:** Nebulae  
**Theme type:** Free / Tumblr use  
**Description:** This theme features Uraraka Ochako from Boku no Hero Academia, and consists of a sidebar, header image, sticky custom links bar, contained posts, and 3 updates tabs in the form of stars.  
**Author:** @&hairsp;glenthemes  

**Release date:** 2019-01-30  
**Last updated:** 2023-05-08

**Post:** [glenthemes.tumblr.com/post/170293555634](https://glenthemes.tumblr.com/post/170293555634)  
**Preview:** [glenthpvs.tumblr.com/nebulae](https://glenthpvs.tumblr.com/nebulae)  
**Download:** [pastebin.com/Fm7NtuUw](https://pastebin.com/Fm7NtuUw)  
**Guide:** [glenthemes.tumblr.com/nebulae](https://glenthemes.tumblr.com/nebulae)
